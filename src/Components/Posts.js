import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import respinaData from "../data/respinaData.json";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

const CardMediaStyled = withStyles({
  image: {
    width: "100%",
    height: "auto",
  },
})(CardMedia);
const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(8),
  },
  
  media: {
    height: 240,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  modalBox: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 4, 3),
    borderRadius: "10px",
    width: "40rem",
    fontSize: "13px",
    fontWeight: "bold !important",
  },
  categoryItem: {
    textAlign: "left",
  },
  userFullName: {
    textAlign: "right !important",
    lineHight: "50px",
  },
  bookDetailImag: {
    width: "100%",
  },
  btnModal: {
    backgroundColor: "#fff",
    border: "none",
  },
}));
const Posts = ({ posts }) => {
  const classes = useStyles();
  const [authorId, setAuthorId] = useState(null);
  const [book, setBook] = useState();
  const [open, setOpen] = React.useState(false);

  const getDetailBook = (id) => {
    respinaData.Author.map((item, index) => {
      item.id == id && setBook(item);
    });
  };
  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = (id) => {
    setAuthorId(id);
    getDetailBook(id);
    setOpen(true);
  };

  return (
    <Grid container spacing={3}>
      {posts.map((item, index) => {
        return (
          <Grid item md={3} xs={12} key={index}>
            <Card className={classes.root}>
              <button
                type="button"
                onClick={(e) => handleOpen(item.id)}
                style={{ width: "100%" }}
                className={classes.btnModal}
              >
                <CardActionArea>
                  {item.Book.map((bookItem, i) => {
                    return (
                      <>
                        <CardMediaStyled
                          key={i}
                          className={classes.media}
                          image={bookItem.image}
                        />

                        <p>{bookItem.title}</p>
                        <p>{item.full_name}</p>
                      </>
                    );
                  })}
                </CardActionArea>
              </button>
            </Card>
          </Grid>
        );
      })}

      <div>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={open}>
            <div className={classes.modalBox}>
              <div id="transition-modal-description">
                <div>
                  {book && (
                    <>
                      <Grid item sm={12}>
                        <Grid container spacing={3}>
                          <Grid item md={3}>
                            <img
                              src={book.Book[0].image}
                              className={classes.bookDetailImag}
                              alt=""
                            />
                          </Grid>
                          <Grid item md={9}>
                            <p>کتاب {book.Book[0].title}</p>
                            <span>نویسنده : {book.full_name}</span>
                            <p>مترجم : {book.Book[0].translator}</p>
                          </Grid>
                        </Grid>
                        <Grid item sm={12}>
                          <p>بخشی از کتاب {book.Book[0].title}</p>
                          <p>{book.Book[0].summary}</p>
                        </Grid>
                      </Grid>
                    </>
                  )}
                </div>
              </div>
            </div>
          </Fade>
        </Modal>
      </div>
    </Grid>
  )
}

export default Posts;
