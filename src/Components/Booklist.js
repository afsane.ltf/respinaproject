import React, { useState,useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import respinaData from "../data/respinaData.json";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Checkbox from "@material-ui/core/Checkbox";
import { connect } from "react-redux";
import Posts from './Posts'
import Pagination1 from "./Pagination";
import PersonIcon from "@material-ui/icons/Person";
const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(8),
  },
  title: {
    textAlign: "center",
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(8),
    "& h1": {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
    },
  },
  banner: {
    backgroundImage: "url(/images/banner.png)",
    backgroundSize: "contain",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    height: "400px",
    width: "100%",
    [theme.breakpoints.down("md")]: {
      display: "none !important",
      border: "solid 1px red",
    },
  },
  aboutBox: {
    textAlign: "center",
    border: `1px solid #e4e4e4`,
    padding: theme.spacing(5),
    "& h3": {
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2),
    },
  },
  media: {
    height: 240,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  modalBox: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 4, 3),
    borderRadius: "10px",
    width: "40rem",
    fontSize: "13px",
    fontWeight: "bold !important",
  },
  categoryItem: {
    textAlign: "left",
  },
  userFullName: {
    textAlign: "right !important",
    lineHeight: "70px",
    [theme.breakpoints.down("lg")]: {
      textAlign: "center",
    },
  },
  bookDetailImag: {
    width: "100%",
  },
  btnModal: {
    backgroundColor: "#fff",
    border: "none",
  },
  logo: {
    [theme.breakpoints.down("md")]: {
      textAlign: "center",
    },
  },
  wrapperBanner: {
    height: "4rem !important",
    [theme.breakpoints.down("md")]: {
      height: "7rem !important",
    },
  },
}));
export  function BookList(props) {
const classes = useStyles();
const [currentPage, setCurrentPage] = useState(1)
const [postsPerPage, setpostsPerPage] = useState(4);
const [posts, setPosts] = useState([]);

  
   useEffect(() => {
   
     setPosts(respinaData.Author);
   });

const indexOfLastPost = currentPage * postsPerPage;
const indexOfFirstPost = indexOfLastPost - postsPerPage;
const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost)
const handleCheckBox = (id) => {
  alert(id);
};
const paginate = (pageNumber) => {
  setCurrentPage(pageNumber);
}
  return (
    <div className={classes.root}>
      <Container maxWidth="lg">
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="stretch"
          spacing={3}
        >
          <Grid item xs={12}>
            <Grid container className={classes.wrapperBanner}>
              <Grid item xs={6}>
                <p className={classes.logo}>
                  <a href="/">
                    <img src="images/logo.png" alt="logo" />
                  </a>
                </p>
              </Grid>
              <Grid item xs={6}>
                <p className={classes.userFullName}>
                  <PersonIcon style={{ verticalAlign: "middle" }} />
                  {props.user}
                </p>
              </Grid>
            </Grid>
          </Grid>

          <Grid container>
            <p className={classes.banner}></p>
          </Grid>

          <Grid item xs={12} md={3}>
            <div className={classes.aboutBox}>
              <Typography variant="h6" component="h3" color="secondary">
                دسته بندی
              </Typography>

              {respinaData.Category.map((item, index) => {
                return (
                  <Typography
                    variant="body2"
                    gutterBottom
                    className={classes.categoryItem}
                  >
                    <Checkbox
                      inputProps={{
                        "aria-label": "uncontrolled-checkbox",
                      }}
                      onChange={(e) => handleCheckBox(item.id)}
                    />
                    <span>{item.title}</span>
                  </Typography>
                );
              })}
            </div>
          </Grid>

          <Grid item md={9}>
            <div className={classes.aboutBox}>
              <Posts posts={currentPosts} />
              <Pagination1
                postsPerPage={postsPerPage}
                totalPosts={posts.length}
                paginate={paginate}
              />
            </div>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
const mapStateToProps = (state) => ({
  user: state.setname.userInfo,
});
export default connect(mapStateToProps, null)(BookList);