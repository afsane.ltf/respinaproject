import React from "react";
import { makeStyles } from "@material-ui/core/styles";
const useStyles = makeStyles((theme) => ({
  pagination: {
    float: "right",
    listStyleType: "none",
    padding: "4px",
  },
  number:{
      cursor:'pointer',
      border:'solid 1px',
      padding:'2px 6px'
  }
}));
const Pagination1 = ({ postsPerPage, totalPosts, paginate }) => {
    const classes = useStyles();
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <div>
      {pageNumbers.map((number) => {
        return (
          <li key={number} className={classes.pagination}>
            <span
              onClick={() => paginate(number)}
              href="#"
              className={classes.number}
            >
              {number}
            </span>
          </li>
        );
      })}
    </div>
  );
};
export default Pagination1