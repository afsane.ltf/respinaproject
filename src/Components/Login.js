import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import respinaData from "../data/respinaData.json";
import { toastr } from "react-redux-toastr";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";
import { withStyles } from "@material-ui/core/styles";
import { setName } from "../actions/actionCreator";
import { connect } from "react-redux";

const TextFieldStyled = withStyles({
  root: {
    marginBottom: "1rem",
  },
})(TextField);
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "10rem",
    direction: "rtl important",
    textAlign: "right",
  },
  error: {
    fontWeight: "bold",
    fontSize: "12px",
    color: "red",
    marginBottom: "50px",
  },
}));

export function Login(props) {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState([]);
  const [emailError, setEmailError] = useState(true);
  const [passwordError, setPasswordError] = useState(false);
  const userEmail = respinaData.User.email;
  const userPassword = respinaData.User.password;

  const onchangeEmail = (e) => {
    setEmail(e.target.value);

    var regexEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    !regexEmail.test(e.target.value)
      ? setEmailError(false)
      : setEmailError(true);
  };
  const onchangePassword = (e) => {
    setPassword(e.target.value);
    password.length < 5 ? setPasswordError(true) : setPasswordError(false);
  };
  const submitLoginForm = () => {
    if (email === userEmail && password === userPassword) {
      toastr.success("شما با موفقیت وارد شدید");
      props.history.push("/Booklist");
      props.setName({ userInfo: respinaData.User.full_name });
    } else {
      toastr.error("نام کاربری یا رمز ورود اشتباه است");
    }

    // if (email == '') {setEmailError(true)};
  };

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Container fixed>
        <Grid container spacing={3} className={classes.container}>
          <Grid item xs={12} md={4}>
            <Paper className={classes.paper}>
              <div dir="rtl">
                <form noValidate autoComplete="off">
                  <Grid item xs={12}>
                    <TextFieldStyled
                      id="outlined-basic"
                      label="ایمیل"
                      variant="outlined"
                      value={email}
                      type="email"
                      onChange={onchangeEmail}
                      fullWidth
                    />

                    {!emailError && (
                      <span className={classes.error}>
                        ایمیل معتبر نمی باشد
                      </span>
                    )}
                  </Grid>

                  <Grid item xs={12}>
                    <TextFieldStyled
                      id="outlined-basic"
                      label="رمز عبور"
                      variant="outlined"
                      value={password}
                      type="password"
                      onChange={onchangePassword}
                      fullWidth
                    />

                    {passwordError && (
                      <span className={classes.error}>
                        رمز عبور نباید کمتر از 6 کاراکتر باشد
                      </span>
                    )}
                  </Grid>

                  <Grid item xs={12}>
                    <Button
                      variant="contained"
                      color="primary"
                      disableElevation
                      onClick={submitLoginForm}
                      fullWidth
                    >
                      ورود
                    </Button>
                  </Grid>
                </form>
              </div>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
const mapDispatchToProps = { setName };

export default connect(null, mapDispatchToProps)(Login);
