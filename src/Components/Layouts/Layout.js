import React from "react";
import ReduxToastr from "react-redux-toastr";
import "react-redux-toastr/lib/css/react-redux-toastr.min.css";
import {create} from 'jss';
import rtl from 'jss-rtl';
import {StylesProvider, jssPreset, ThemeProvider} from '@material-ui/core/styles';
import {createMuiTheme} from "@material-ui/core/styles";
import "../../style/styles.css";
const TIMEOUT = 400;

const theme = createMuiTheme({
  direction: "rtl",
  palette: {
    common: {
      black: "#000",
      white: "#fff",
    },
    background: {
      //paper: "rgba(68, 177, 233, 1)",
      default: "#fafafa",
    },
    primary: {
      light: "rgba(112, 112, 112, 1)",
      main: "rgba(40, 40, 40, 1)",
      dark: "rgba(0, 0, 0, 1)",
      contrastText: "#fff",
    },
    secondary: {
      light: "rgba(122, 190, 225, 1)",
      main: "rgba(68, 177, 233, 1)",
      dark: "rgba(30, 160, 229, 1)",
      contrastText: "#fff",
    },
    error: {
      light: "#BD3F3F",
      main: "#f44336",
      dark: "#d32f2f",
      contrastText: "#fff",
    },
    text: {
      primary: "rgba(37, 37, 37, 0.87)",
      secondary: "rgba(0, 0, 0, 0.54)",
      disabled: "rgba(0, 0, 0, 0.38)",
      hint: "rgba(0, 0, 0, 0.38)",
    },
  },
});

const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

const layoutStyle = {
  margin: 0,
  padding: 0,
};

const Layout = (props) => (
  <div>
    <StylesProvider jss={jss}>
      <ThemeProvider theme={theme}>
        <div style={layoutStyle}>
          <ReduxToastr
            timeOut={4000}
            newestOnTop={false}
            preventDuplicates
            position="top-left"
            getState={(state) => state.toastr} // This is the default
            transitionIn="fadeIn"
            transitionOut="fadeOut"
            progressBar
            closeOnToastrClick
          />
         
          {props.children}
        
        </div>
      </ThemeProvider>
    </StylesProvider>
    
  
    <style jsx global>{`
      .redux-toastr .toastr .rrt-middle-container {
        font-family: iranyekan;
        font-size: 13px;
      }
      .redux-toastr .toastr .rrt-left-container {
        right: 14px;
      }
      .redux-toastr .toastr.rrt-success {
        display: flex;
        align-items: center;
        justify-content: center;
        color: #fff;
      }
      .redux-toastr .toastr .rrt-middle-container {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 100%;
        margin-left: 0;
      }
      .redux-toastr .toastr .rrt-title {
        text-align: right;
      }
    `}</style>
  </div>
);

export default Layout;
