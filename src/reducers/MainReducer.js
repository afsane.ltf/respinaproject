import { combineReducers } from 'redux'
import setname from "./SetNameReducer";

import { reducer as toastrReducer } from "react-redux-toastr";
export default combineReducers({
    setname,
    toastr: toastrReducer,
})