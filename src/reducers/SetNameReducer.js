import { SET_NAME } from "../actions/actionsTypes";
const initial = {
  userInfo:'',
 
}


const SetNameReducer = (state = initial, action) => {
    switch (action.type) {
      case SET_NAME:
        return Object.assign({}, state, action.payload);
    
      default:
        return state;
    }
}

export default SetNameReducer;
