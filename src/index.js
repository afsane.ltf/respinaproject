import * as serviceWorker from "./serviceWorker";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import MainReducer from "./reducers/MainReducer";
import { loadState, saveState } from "./localStorage";
import configureStore from "./configureStore";
import { PersistGate } from "redux-persist/integration/react";
import { Router, Route, Link } from "react-router-dom";
import Login from "./Components/Login";
import Booklist from "./Components/Booklist";
import history from "./history";
import Layout from "./Components/Layouts/Layout";
const persistedState = loadState();

const { store, persistor } = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <PersistGate loading={null} persistor={persistor}>
        <Layout>
        <Route exact path="/" component={Login} />
        <Route path="/Booklist" component={Booklist} />
        </Layout>
      </PersistGate>
    </Router>
  </Provider>,
  document.getElementById("root")
)
serviceWorker.unregister();
