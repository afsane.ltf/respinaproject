import { applyMiddleware, createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import {createLogger} from 'redux-logger'

import rootReducer from './reducers/MainReducer'

const persistConfig = {
  key: "root",
  storage,
  //whitelist: ['todos'] // only navigation will be persisted
  whitelist: ['setname'] // only navigation will be persisted
};

const logger = createLogger({
  collapsed :false,
  level: 'warn',
});

const persistedReducer = persistReducer(persistConfig, rootReducer)

export default () => {
  let store = createStore(persistedReducer,applyMiddleware(logger))
  let persistor = persistStore(store)
  return { store, persistor }
}