
import { SET_NAME } from "./actionsTypes";
export const setName = (value) => ({
         type: SET_NAME,
         payload: value,
 })
